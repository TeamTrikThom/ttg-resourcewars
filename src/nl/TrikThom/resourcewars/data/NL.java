package nl.TrikThom.resourcewars.data;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class NL {
  static NL instance = new NL();
  Plugin p;
  FileConfiguration nl;
  File nlfile;
  
  public static NL getInstance() {
    return instance;
  }
  
  public void setup(Plugin plugin) {
    this.nlfile = new File(plugin.getDataFolder(), "/Lang/NL.yml");
    if (!this.nlfile.exists()) {
      try
      {
        this.nlfile.createNewFile();
        
      }
      catch (IOException e)
      {
        Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand NL.yml niet aanmaken!");
      }
    }
    this.nl = YamlConfiguration.loadConfiguration(this.nlfile);
    
    registerDefaults();
  }
  
  public FileConfiguration getNLData() {
    return this.nl;
  }
  
  public void saveNLData() {
    try
    {
      this.nl.save(this.nlfile);
    }
    catch (IOException e)
    {
      Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand NL.yml niet opslaan!");
    }
  }
  
  public void reloadNLData() {
    this.nl = YamlConfiguration.loadConfiguration(this.nlfile);
  }
  
  public void registerDefaults() {
	  
	  this.nl.addDefault("header", "�3============�a[�6ResourceWars�a]�3============");
	  this.nl.addDefault("prefix", "�a[�6ResourceWars�a]");
	  this.nl.addDefault("color", "�3");
	  this.nl.addDefault("accent", "�a");
	  
	  
	  this.nl.addDefault("help.help", "�a/rw help �7- krijg alle commands opgelijst");
	  this.nl.addDefault("help.reset", "�a/rw reset �7- reset de playerdata van een speler");
	  this.nl.addDefault("help.setlang", "�a/rw settaal <NL/EN> �7- verander de speeltaal");
	  this.nl.addDefault("help.peace", "�a/vrede <natie> �7- leef in vrede met de andere natie");
	  this.nl.addDefault("help.war", "�a/oorlog <natie> �7- verklaar de oorlog aan de andere natie");
	  this.nl.addDefault("help.booster", "�a/booster �7- activeer een booster voor iedereen in je natie");
	  
  }
  
  
}
