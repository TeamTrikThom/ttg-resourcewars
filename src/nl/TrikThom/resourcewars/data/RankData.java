package nl.TrikThom.resourcewars.data;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class RankData {
  static RankData instance = new RankData();
  Plugin p;
  FileConfiguration RankData;
  File pdfile;
  
  public static RankData getInstance() {
    return instance;
  }
  
  public void setup(Plugin p) {
    this.pdfile = new File(p.getDataFolder(), "ranks.yml");
    if (!this.pdfile.exists()) {
      try
      {
        this.pdfile.createNewFile();
        
      }
      catch (IOException e)
      {
        Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand ranks.yml niet aanmaken!");
      }
    }
    this.RankData = YamlConfiguration.loadConfiguration(this.pdfile);
  }
  
  public FileConfiguration getRankData() {
    return this.RankData;
  }
  
  public void saveRankData() {
    try
    {
      this.RankData.save(this.pdfile);
    }
    catch (IOException e)
    {
      Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand ranks.yml niet opslaan!");
    }
  }
  
  public void reloadRankData() {
    this.RankData = YamlConfiguration.loadConfiguration(this.pdfile);
  }
  
  
}
