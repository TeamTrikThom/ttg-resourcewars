package nl.TrikThom.resourcewars.data;

import nl.TrikThom.resourcewars.main.Main;

public class Config {

	public static void registerDefaults() {
		Main.plugin.getConfig().addDefault("defaultLang", "nl");
		Main.plugin.getConfig().addDefault("nations.Modreidan.kleur", "�c");
		Main.plugin.getConfig().addDefault("nations.Hercanos.kleur", "�a");
		Main.plugin.getConfig().addDefault("nations.Otror.kleur", "�6");
		Main.plugin.getConfig().addDefault("nations.Zecrana.kleur", "�b");
	}
	
}
