package nl.TrikThom.resourcewars.data;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class EN {
  static EN instance = new EN();
  Plugin p;
  FileConfiguration en;
  File enfile;
  
  public static EN getInstance() {
    return instance;
  }
  
  public void setup(Plugin plugin) {
    this.enfile = new File(plugin.getDataFolder(), "/Lang/EN.yml");
    if (!this.enfile.exists()) {
      try
      {
        this.enfile.createNewFile();
        
      }
      catch (IOException e)
      {
        Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand EN.yml niet aanmaken!");
      }
    }
    this.en = YamlConfiguration.loadConfiguration(this.enfile);
    registerDefaults();
  }
  
  public FileConfiguration getENData() {
    return this.en;
  }
  
  public void saveENData() {
    try
    {
      this.en.save(this.enfile);
    }
    catch (IOException e)
    {
      Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand EN.yml niet opslaan!");
    }
  }
  
  public void reloadENData() {
    this.en = YamlConfiguration.loadConfiguration(this.enfile);
  }
  
  public void registerDefaults() {
	  
	  this.en.addDefault("header", "�3============�a[�6ResourceWars�a]�3============");
	  
	  
	  this.en.addDefault("help.help", "�a/rw help �7- get all commands");
	  this.en.addDefault("help.reset", "�a/rw reset �7- reset the playerdata from a player");
	  this.en.addDefault("help.peace", "�a/peace <nation> �7- live in peace in your nation");
	  this.en.addDefault("help.war", "�a/war <nation> �7- declare war to a nation");
	  this.en.addDefault("help.booster", "�a/booster �7- activate a booster for everyone in your nation");
  }
  
}
