package nl.TrikThom.resourcewars.data;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class Nations {
  static Nations instance = new Nations();
  Plugin p;
  FileConfiguration ns;
  File nsfile;
  
  public static Nations getInstance() {
    return instance;
  }
  
  public void setup(Plugin plugin) {
    this.nsfile = new File(plugin.getDataFolder(), "/Data/nations.yml");
    if (!this.nsfile.exists()) {
      try
      {
        this.nsfile.createNewFile();
        
      }
      catch (IOException e)
      {
        Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand nations.yml niet aanmaken!");
      }
    }
    this.ns = YamlConfiguration.loadConfiguration(this.nsfile);
    
    registerDefaults();
  }
  
  public FileConfiguration getNationsData() {
    return this.ns;
  }
  
  public void saveNationsData() {
    try
    {
      this.ns.save(this.nsfile);
    }
    catch (IOException e)
    {
      Bukkit.getServer().getLogger().severe(ChatColor.RED + "Kan het bestand nations.yml niet opslaan!");
    }
  }
  
  public void reloadNationsData() {
    this.ns = YamlConfiguration.loadConfiguration(this.nsfile);
  }
  
  public void registerDefaults() {
	  this.ns.addDefault("nations", "�3============�a[�6ResourceWars�a]�3============");
	  
  }
  
  
}
