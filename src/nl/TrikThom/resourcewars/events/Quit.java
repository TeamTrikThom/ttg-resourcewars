package nl.TrikThom.resourcewars.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Quit implements Listener {

	
	@EventHandler
	public void onQuit (PlayerQuitEvent event) {
		
		event.setQuitMessage("");
		
		
		
	}
	
	
	
	public String replaceColors (String message) {
		return message.replaceAll("(?i)&([a-r0-9])", "\u00A7$1");
	}
}
