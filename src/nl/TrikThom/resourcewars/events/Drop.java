package nl.TrikThom.resourcewars.events;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

public class Drop implements Listener {

	@EventHandler
	public void PlayerDropItemEvent(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().getType() == Material.STAINED_GLASS_PANE) {
			event.setCancelled(true);

		}
	}
	
	@EventHandler
    public void onItemSpawn(ItemSpawnEvent event) {
        boolean n = event.getEntity().getItemStack().getType() == Material.STAINED_GLASS_PANE;
            if(n == true){
                event.getEntity().remove();
               
            }   
    }
	
}
