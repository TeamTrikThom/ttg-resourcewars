package nl.TrikThom.resourcewars.events;

import org.bukkit.Achievement;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;

import nl.TrikThom.resourcewars.menus.MainMenu;

@SuppressWarnings("deprecation")
public class OpenInv implements Listener {
	
	@EventHandler
    public void onInvEvent(InventoryOpenEvent e) {
        
    }

	@EventHandler
	public void onInventoryOpenEvent(PlayerAchievementAwardedEvent event){
	    if(event.getAchievement().equals(Achievement.OPEN_INVENTORY)){
	        event.setCancelled(true);
	        Player player = event.getPlayer();
	        if (player.getOpenInventory().getType() != InventoryType.CREATIVE) {
	        	MainMenu.openMenu(player);
	        }
	        
	    }
	}

}
