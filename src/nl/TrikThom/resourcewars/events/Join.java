package nl.TrikThom.resourcewars.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import nl.TrikThom.resourcewars.API.API;
import nl.TrikThom.resourcewars.cosmetics.Boosters;
import nl.TrikThom.resourcewars.cosmetics.NameTag;
import nl.TrikThom.resourcewars.data.PlayerData;
import nl.TrikThom.resourcewars.invs.StartInv;
import nl.TrikThom.resourcewars.main.Main;
import nl.TrikThom.resourcewars.main.UpdateChecker;
import nl.TrikThom.resourcewars.menus.MainMenu;
import nl.TrikThom.resourcewars.menus.StartMenu;


public class Join implements Listener {
	private static PlayerData pd = PlayerData.getInstance();
	
	@EventHandler
	public void onJoin (PlayerJoinEvent event) {
		Player player = event.getPlayer();
		pd.getPlayerData().addDefault(player.getUniqueId() + ".level", Integer.valueOf(0));
		pd.getPlayerData().addDefault(player.getUniqueId() + ".state", Integer.valueOf(0));
		pd.getPlayerData().addDefault(player.getUniqueId() + ".nation", String.valueOf("Geen"));
		pd.getPlayerData().addDefault(player.getUniqueId() + ".lang", String.valueOf(Main.plugin.getConfig().getString("defaultLang")));
	    pd.getPlayerData().options().copyDefaults(true);
	    pd.savePlayerData();
	    event.setJoinMessage("");
	    
	    
	    
	    
	    
		joined(player);
		
		
		
		Boosters.bossBarSetup(player);
		
		
		NameTag.registerNameTag(player);
		
		if (player.isOp()) {
			if (UpdateChecker.check() == false) {
				player.sendMessage("Deze versie is niet de laatste!");
				player.sendMessage(API.lang(player, "prefix") + API.lang(player, "color") + "De " + API.lang(player, "accent") + " ");
			}
		}
		
		
		
	}

	public void joined(Player player) {
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
			@Override
        	public void run() {
				if(API.getState(player) == 0) {
					StartInv.getInv(player, true);
        			StartMenu.openMenu(player);
                }
				
				MainMenu.lockItems(player);
				
				
            }
        }, 20L);
	}
	public String replaceColors (String message) {
		return message.replaceAll("(?i)&([a-r0-9])", "\u00A7$1");
	}
	
}
