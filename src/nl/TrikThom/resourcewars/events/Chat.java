package nl.TrikThom.resourcewars.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import nl.TrikThom.resourcewars.API.API;
import nl.TrikThom.resourcewars.main.Main;



public class Chat implements Listener{

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		int level = API.getLevel(player);
		if(API.getNation(player) == "Geen") {
			player.sendMessage("�cJe moet eerst een natie kiezen!");
			event.setCancelled(true);
		}
		event.setFormat("�f[�7Level " + level + "�f] �f["+ Main.plugin.getConfig().getString("nations." + API.getNation(player) + ".kleur")  + API.getNation(player) + "�f] �7" + player.getName() + "�7: �7%2$s");

	}
	
	
	public String replaceColors (String message) {
		return message.replaceAll("(?i)&([a-r0-9])", "\u00A7$1");
	}
	
}
