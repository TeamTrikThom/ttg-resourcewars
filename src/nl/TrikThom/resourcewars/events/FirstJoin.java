package nl.TrikThom.resourcewars.events;

import java.sql.SQLException;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import nl.TrikThom.resourcewars.API.API;
import nl.TrikThom.resourcewars.menus.StartMenu;

public class FirstJoin implements Listener{

	
	@EventHandler
	public void onMove (PlayerMoveEvent  event) throws SQLException {
		Player player = event.getPlayer();
		if(API.getState(player) == 0) {
			player.teleport(player);
			StartMenu.openMenu(player);
		}
	}
}
