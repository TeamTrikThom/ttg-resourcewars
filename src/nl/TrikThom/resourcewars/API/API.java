package nl.TrikThom.resourcewars.API;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import nl.TrikThom.resourcewars.cosmetics.NameTag;
import nl.TrikThom.resourcewars.data.EN;
import nl.TrikThom.resourcewars.data.NL;
import nl.TrikThom.resourcewars.data.Nations;
import nl.TrikThom.resourcewars.data.PlayerData;
import nl.TrikThom.resourcewars.data.RankData;
import nl.TrikThom.resourcewars.main.Main;

public class API {
	
    private static PlayerData pd = PlayerData.getInstance();
    private static RankData rd = RankData.getInstance();
    private static NL nl = NL.getInstance();
    private static EN en = EN.getInstance();
    private static Nations ns = Nations.getInstance();
  
    //SET VARIABLES
  			//PLAYER DATA
    public static void setState(Player player, int state) {
    	pd.getPlayerData().set(player.getUniqueId() + ".state", Integer.valueOf(state));
    	pd.savePlayerData();
    }
  
    public static void setNation(Player player, String nation) {
    	pd.getPlayerData().set(player.getUniqueId() + ".nation", String.valueOf(nation));
    	pd.savePlayerData();
    	NameTag.registerNameTag(player);
    }
  
    public static void setLevel(Player player, int level) {
    	pd.getPlayerData().set(player.getUniqueId() + ".level", Integer.valueOf(level));
    	pd.savePlayerData();
    }
  
  			//RANKDATA
    public static void setRank(Player player, String rank) {
    	ArrayList<String> list = (ArrayList<String>) rd.getRankData().getStringList("rank");
    	if(!list.contains(player.getName())) {
    		list.add(player.getName());
    	}
    	rd.getRankData().set("rank", list);
    	rd.saveRankData();
    }
  
  
    //GET VARIABLES
    public static int getState(Player player) {
    	return pd.getPlayerData().getInt(player.getUniqueId() + ".state");
    }
  
    public static String getNation(Player player) {
    	return pd.getPlayerData().getString(player.getUniqueId() + ".nation");
    }
  
    public static int getLevel(Player player) {
    	return pd.getPlayerData().getInt(player.getUniqueId() + ".level");
    }
  
  
  
    //SETUP VARIABLES
    public static void setupPlayerData(Player player) {
    	pd.getPlayerData().set(player.getUniqueId() + ".state", 0);
    	pd.getPlayerData().set(player.getUniqueId() + ".nation", "Geen");
    	pd.savePlayerData();
    }
  
  
    //RESET VARIABLES
    public static void resetPlayerData(Player player) {
    	pd.getPlayerData().set(player.getUniqueId() + ".state", 0);
    	pd.getPlayerData().set(player.getUniqueId() + ".nation", "Geen");
    	pd.savePlayerData();
    }
    
    
    //WAR
    public static void checkSetWar(Player player, String nationToAttack, String mode) {
    	String nation = pd.getPlayerData().getString(player.getUniqueId() + ".nation");
    	if(!nation.equalsIgnoreCase(nationToAttack)) {
    		if(mode.equalsIgnoreCase("aan")) {
    			ns.getNationsData().set(nation + ".war", nationToAttack);
        		ns.getNationsData().set(nationToAttack + ".war", nation);
    		}
    		else if(mode.equalsIgnoreCase("uit")) {
    			ns.getNationsData().set(nation + ".war", null);
        		ns.getNationsData().set(nationToAttack + ".war", null);
    		}
    		ns.saveNationsData();
    		
    		
    	}
    }
    
    
    
    //LANGUAGE
    public static String lang(Player player, String key) {
    	String userLang = pd.getPlayerData().getString(player.getUniqueId() + ".lang");
    	String lang = Main.plugin.getConfig().getString(key);
    	if(userLang.equalsIgnoreCase("nl")) {
    		lang = nl.getNLData().getString(key);
    	}
    	if(userLang.equalsIgnoreCase("en")) {
    		lang = en.getENData().getString(key);
    	}
    	
    	return lang;
    	
    }
    public static void setLang(Player player, String lang) {
    	
    	pd.getPlayerData().set(player.getUniqueId() + ".lang", String.valueOf(lang));
    	pd.savePlayerData();
    }  
}
