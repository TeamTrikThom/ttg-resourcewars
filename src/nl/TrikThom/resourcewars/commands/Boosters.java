package nl.TrikThom.resourcewars.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import nl.TrikThom.resourcewars.menus.BoosterMenu;

public class Boosters implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player)sender;
		if (cmd.getName().equalsIgnoreCase("boosters")) {
			BoosterMenu.openMenu(player);
			return true;
		}
		
		return false;
	}
	
}
