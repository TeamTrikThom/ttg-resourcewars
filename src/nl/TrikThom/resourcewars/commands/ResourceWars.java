package nl.TrikThom.resourcewars.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import nl.TrikThom.resourcewars.API.API;

public class ResourceWars implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player)sender;
		if (cmd.getName().equalsIgnoreCase("rw") || cmd.getName().equalsIgnoreCase("resourcewars")) {
			if(args.length == 0) {
				player.sendMessage(API.lang(player, "header"));
				player.sendMessage(API.lang(player, "help.help"));
				player.sendMessage(API.lang(player, "help.reset"));
				player.sendMessage(API.lang(player, "help.setlang"));
				player.sendMessage(API.lang(player, "help.peace"));
				player.sendMessage(API.lang(player, "help.war"));
				player.sendMessage(API.lang(player, "help.booster"));
				
			}
			else {
				if (args[0].equalsIgnoreCase("setlang") || args[0].equalsIgnoreCase("settaal")) {
					if(args.length != 2) {
						player.sendMessage(API.lang(player, "prefix") + API.lang(player, "color") + "" + "taal niet veranderd");
					}
					else {
						player.sendMessage(API.lang(player, "prefix") + API.lang(player, "color") + "taal veranderd naar" + API.lang(player, "accent") + " " + args[1].toLowerCase());
						API.setLang(player, args[1]);
					}
				}
				else {
					player.sendMessage(API.lang(player, "prefix") + API.lang(player, "color") + "" + "/rw help");
				}
			}
			return true;
		}
		return false;
	}
}