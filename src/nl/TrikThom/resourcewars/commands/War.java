package nl.TrikThom.resourcewars.commands;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import nl.TrikThom.resourcewars.API.API;

public class War implements CommandExecutor {
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player)sender;
		if (cmd.getName().equalsIgnoreCase("war") || cmd.getName().equalsIgnoreCase("oorlog")) {
			if(args.length == 2) {
				API.checkSetWar(player, args[0], args[1]);
				Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
				String nation = API.getNation(player);
				Team team = scoreboard.getTeam(nation);
				for (OfflinePlayer name : team.getPlayers()) {
				    if (!name.isOnline()) {
				    	continue;
				    }
				    if(args[1].equalsIgnoreCase("aan")) {
				    	name.getPlayer().sendMessage("Je hebt nu oorlog!");
				    }
				    if(args[1].equalsIgnoreCase("uit")) {
				    	name.getPlayer().sendMessage("Je hebt nu geen oorlog meer!");
				    }
				    
				}
			}
			else {
				player.sendMessage("/oorlog <natie> <aan/uit>");
			}
			return true;
		}
		return false;
	}
}