package nl.TrikThom.resourcewars.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import nl.TrikThom.resourcewars.API.API;

public class Reset implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player player = (Player)sender;
		if (cmd.getName().equalsIgnoreCase("reset")) {
			if (!player.isOp()) {
				player.sendMessage("§f[§c§lTTG§f] Je hebt geen permissie om deze commando uit te voeren!");
				return true;
			}
			player.sendMessage("§f[§c§lTTG§f] De PlayerData is gereset!");
			API.resetPlayerData(player);
			return true;
		}
		return false;
	}
}