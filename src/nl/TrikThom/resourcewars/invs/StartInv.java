package nl.TrikThom.resourcewars.invs;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class StartInv implements Listener {

	public static void getInv(Player player, boolean startinv) {
		if(startinv) {
			ItemStack LockedItem = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 15);
		    ItemMeta LockedItemMeta = LockedItem.getItemMeta();
		    LockedItemMeta.setDisplayName("�cLocked");
		    LockedItem.setItemMeta(LockedItemMeta);
		    //player.getInventory().clear();
		    player.getInventory().setHelmet(new ItemStack (Material.AIR));
		    player.getInventory().setChestplate(new ItemStack (Material.AIR));
		    player.getInventory().setLeggings(new ItemStack (Material.AIR));
		    player.getInventory().setBoots(new ItemStack (Material.AIR));
		    for(int i = 9; i<36; i++) {
		    	player.getInventory().setItem(i, LockedItem);
		    }
		}
		
	}
}
