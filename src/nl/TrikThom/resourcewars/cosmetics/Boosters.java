package nl.TrikThom.resourcewars.cosmetics;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

public class Boosters {

	public static BossBar walkBoosterBar;
	public static BossBar jumpBoosterBar;
	
	public static void bossBarSetup(Player player) {
		
		walkBoosterBar = Bukkit.createBossBar("Walk Booster", BarColor.YELLOW, BarStyle.SEGMENTED_10, new BarFlag[0]);
		jumpBoosterBar = Bukkit.createBossBar("Jump Booster", BarColor.YELLOW, BarStyle.SEGMENTED_10, new BarFlag[0]);
		
	}
	public static void bossBarChange(Player player, String bar) {
		
		walkBoosterBar = Bukkit.createBossBar("Walk Booster", BarColor.YELLOW, BarStyle.SEGMENTED_10, new BarFlag[0]);
		jumpBoosterBar = Bukkit.createBossBar("Jump Booster", BarColor.YELLOW, BarStyle.SEGMENTED_10, new BarFlag[0]);
		
		
		if(bar.equalsIgnoreCase("walkBoosterBar")) {
			walkBoosterBar.setTitle("Walk Booster 10%");
			walkBoosterBar.setProgress(0.1f);
			walkBoosterBar.addPlayer(player);
		}
		
		
		
		
	}
	
}
