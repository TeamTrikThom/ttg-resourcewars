package nl.TrikThom.resourcewars.cosmetics;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import nl.TrikThom.resourcewars.API.API;
import nl.TrikThom.resourcewars.main.Main;

public class NameTag {

	@SuppressWarnings("deprecation")
	public static void registerNameTag(Player player) {
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
		
		String nation = API.getNation(player);
		
		Team team = scoreboard.getTeam(nation);

		if(team == null) {
			team = scoreboard.registerNewTeam(nation);
		}
		if(Main.plugin.getConfig().getString("nations." + API.getNation(player) + ".kleur") == null) {
			team.setPrefix("�7�k");
		}
		else {
			team.setPrefix(Main.plugin.getConfig().getString("nations." + API.getNation(player) + ".kleur"));
		}
		
		team.addPlayer(player);
		
		for(Player all : Bukkit.getOnlinePlayers()) {
			all.setScoreboard(scoreboard);
		}
	}
	
}
