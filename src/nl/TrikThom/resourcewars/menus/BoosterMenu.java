package nl.TrikThom.resourcewars.menus;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import nl.TrikThom.resourcewars.cosmetics.Boosters;

public class BoosterMenu implements Listener {

	
	public static void openMenu(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, "�aBoosters");
		
		ItemStack LockedItem = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 15);
	    ItemMeta LockedItemMeta = LockedItem.getItemMeta();
	    LockedItemMeta.setDisplayName("�aWalk Booster");
	    LockedItem.setItemMeta(LockedItemMeta);
		
	    inv.setItem(3, LockedItem);
		
		player.openInventory(inv);
		
		
	}
	
	@EventHandler
	public void onClickSlot(InventoryClickEvent event) {
		if(event.getCurrentItem() == null) {
			return;
		}
		if (event.getCurrentItem().getType() != Material.AIR) {
			Player player = (Player) event.getWhoClicked();
			if (event.getCurrentItem().getItemMeta().getDisplayName() == "�aWalk Booster") {
				
				Boosters.bossBarChange(player, "walkBoosterBar");
				event.setCancelled(true);
			}
		}
		else {
			return;
		}
		
	}
	
}
