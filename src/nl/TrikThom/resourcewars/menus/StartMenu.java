package nl.TrikThom.resourcewars.menus;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import nl.TrikThom.resourcewars.API.API;

public class StartMenu implements Listener {

	
	public static void openMenu(Player player) {
		
		Inventory inv = Bukkit.createInventory(null, 27, "�eKies een natie!");
		
		ItemStack ChooseNation = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 15);
	    ItemMeta ChooseNationMeta = ChooseNation.getItemMeta();
	    ChooseNationMeta.setDisplayName("�3Kies een natie!");
	    ChooseNation.setItemMeta(ChooseNationMeta);
	    
		ItemStack Modreidan = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 14);
	    ItemMeta ModreidanMeta = Modreidan.getItemMeta();
	    ModreidanMeta.setDisplayName("�cModreidan");
	    Modreidan.setItemMeta(ModreidanMeta);
	    
	    ItemStack Hercanos = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 13);
	    ItemMeta HercanosMeta = Hercanos.getItemMeta();
	    HercanosMeta.setDisplayName("�aHercanos");
	    Hercanos.setItemMeta(HercanosMeta);
	    
	    ItemStack Otror = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 4);
	    ItemMeta OtrorMeta = Otror.getItemMeta();
	    OtrorMeta.setDisplayName("�6Otror");
	    Otror.setItemMeta(OtrorMeta);
	    
	    ItemStack Zecrana = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 3);
	    ItemMeta ZecranaMeta = Zecrana.getItemMeta();
	    ZecranaMeta.setDisplayName("�bZecrana");
	    Zecrana.setItemMeta(ZecranaMeta);
	    
	   
		
	    for(int i = 0; i<27; i++) {
	    	if(i == 10) {
				inv.setItem(i, Modreidan);
			}
			else if(i == 12) {
				inv.setItem(i, Hercanos);
			}
			else if(i == 14) {
				inv.setItem(i, Otror);
			}
			else if(i == 16) {
				inv.setItem(i, Zecrana);
			}
			else {
				inv.setItem(i, ChooseNation);
			}
	    }
	    for(int i = 9; i<36; i++) {
	    	player.getInventory().setItem(i, ChooseNation);
	    }
	    player.openInventory(inv);
		
	}
	
	
	
	
	
	
	
	
	
	@EventHandler
	public void onClickSlot(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		if(event.getCurrentItem() == null) {
			return;
		}
		if (event.getCurrentItem().getType() != Material.AIR) {
			if (event.getInventory().getTitle() == "�eKies een natie!") {
				if (event.getCurrentItem().getItemMeta().getDisplayName() == "�cModreidan") {
					API.setNation(player, "Modreidan");
					API.setState(player, 1);
					player.closeInventory();
					Characteristics.openMenu(player);
				}
				if (event.getCurrentItem().getItemMeta().getDisplayName() == "�aHercanos") {
					API.setNation(player, "Hercanos");
					API.setState(player, 1);
					player.closeInventory();
					Characteristics.openMenu(player);
				}
				if (event.getCurrentItem().getItemMeta().getDisplayName() == "�6Otror") {
					API.setNation(player, "Otror");
					API.setState(player, 1);
					player.closeInventory();
					Characteristics.openMenu(player);
				}
				if (event.getCurrentItem().getItemMeta().getDisplayName() == "�bZecrana") {
					API.setNation(player, "Zecrana");
					API.setState(player, 1);
					player.closeInventory();
					Characteristics.openMenu(player);
				}
				event.setCancelled(true);
			}
			else {
				return;
			}
		}
		else {
			return;
		}
		
	}
}
