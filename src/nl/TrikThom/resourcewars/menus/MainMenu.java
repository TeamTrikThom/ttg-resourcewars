package nl.TrikThom.resourcewars.menus;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MainMenu implements Listener {

	
	public static void openMenu(Player player) {
		
		
		Inventory inv = Bukkit.createInventory(null, 27, "�eRugzak");
		
		ItemStack LockedItem = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 15);
	    ItemMeta LockedItemMeta = LockedItem.getItemMeta();
	    LockedItemMeta.setDisplayName("�cLocked");
	    LockedItem.setItemMeta(LockedItemMeta);
	    
	    ItemStack Settings = new ItemStack(Material.WORKBENCH, 1,(byte) 0);
	    ItemMeta SettingsMeta = Settings.getItemMeta();
	    SettingsMeta.setDisplayName("�3Instellingen");
	    Settings.setItemMeta(SettingsMeta);
	    
	    ItemStack Profile = new ItemStack(Material.PAPER, 1,(byte) 0);
	    ItemMeta ProfileMeta = Profile.getItemMeta();
	    ProfileMeta.setDisplayName("�aProfiel");
	    Profile.setItemMeta(ProfileMeta);
		
		
	    for(int i = 0; i<27; i++) {
	    	inv.setItem(i, LockedItem);
			
	    }
	    lockItems(player);
	    
	    player.openInventory(inv);
		
	}
	
	
	public static void lockItems(Player player) {
		ItemStack LockedItem = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 15);
	    ItemMeta LockedItemMeta = LockedItem.getItemMeta();
	    LockedItemMeta.setDisplayName("�cLocked");
	    LockedItem.setItemMeta(LockedItemMeta);
	    
	    ItemStack Settings = new ItemStack(Material.WORKBENCH, 1,(byte) 0);
	    ItemMeta SettingsMeta = Settings.getItemMeta();
	    SettingsMeta.setDisplayName("�3Instellingen");
	    Settings.setItemMeta(SettingsMeta);
	    
	    ItemStack Profile = new ItemStack(Material.PAPER, 1,(byte) 0);
	    ItemMeta ProfileMeta = Profile.getItemMeta();
	    ProfileMeta.setDisplayName("�aProfiel");
	    Profile.setItemMeta(ProfileMeta);
		
		
		for(int i = 9; i<36; i++) {
	    	player.getInventory().setItem(i, LockedItem);
	    	if(i == 9) {
	    		player.getInventory().setItem(i, Settings);
	    	}
	    	if(i == 10) {
	    		player.getInventory().setItem(i, Profile);
	    	}
	    }
	}
	
	
	
	
	
	
	@EventHandler
	public void onClickSlot(InventoryClickEvent event) {
		if(event.getCurrentItem() == null) {
			return;
		}
		if (event.getCurrentItem().getType() != Material.AIR) {
			String block = event.getCurrentItem().getItemMeta().getDisplayName();
			if (block == "�cLocked" || block == "�3Instellingen" || block == "�aProfiel") {
				event.setCancelled(true);
			}
		}
		else {
			return;
		}
		
	}
}
