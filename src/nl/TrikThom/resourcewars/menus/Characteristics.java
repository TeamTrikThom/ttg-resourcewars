package nl.TrikThom.resourcewars.menus;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Characteristics implements Listener {

	
	public static void openMenu(Player player) {
		Inventory inv = Bukkit.createInventory(null, 27, "�eKies je optie!");
		
		ItemStack ChooseOption = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 15);
	    ItemMeta ChooseOptionMeta = ChooseOption.getItemMeta();
	    ChooseOptionMeta.setDisplayName("�3Kies je optie!");
	    ChooseOption.setItemMeta(ChooseOptionMeta);
	    
	    
	    
	    ItemStack Option1 = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 1);
	    ItemMeta Option1Meta = Option1.getItemMeta();
	    Option1Meta.setDisplayName("�aOptie #1");
	    Option1Meta.setLore(Arrays.asList(new String[] { "�3Deze optie bevat:", "�7- 4x brood" }));
	    Option1.setItemMeta(Option1Meta);
	    
	    ItemStack Option2 = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 1);
	    ItemMeta Option2Meta = Option2.getItemMeta();
	    Option2Meta.setDisplayName("�aOptie #2");
	    Option2Meta.setLore(Arrays.asList(new String[] { "�3Deze optie bevat:", "�7- 4x brood" }));
	    Option2.setItemMeta(Option2Meta);
	    
	    ItemStack Option3 = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 1);
	    ItemMeta Option3Meta = Option3.getItemMeta();
	    Option3Meta.setDisplayName("�aOptie #3");
	    Option3Meta.setLore(Arrays.asList(new String[] { "�3Deze optie bevat:", "�7- 4x brood" }));
	    Option3.setItemMeta(Option3Meta);
	    
	    ItemStack Option4 = new ItemStack(Material.STAINED_GLASS_PANE, 1,(byte) 1);
	    ItemMeta Option4Meta = Option4.getItemMeta();
	    Option4Meta.setDisplayName("�aOptie #4");
	    Option4Meta.setLore(Arrays.asList(new String[] { "�3Deze optie bevat:", "�7- 4x brood" }));
	    Option4.setItemMeta(Option4Meta);
	    
	    
		
	    for(int i = 0; i<27; i++) {
	    	if(i == 10) {
				inv.setItem(i, Option1);
			}
			else if(i == 12) {
				inv.setItem(i, Option2);
			}
			else if(i == 14) {
				inv.setItem(i, Option3);
			}
			else if(i == 16) {
				inv.setItem(i, Option4);
			}
			else {
				inv.setItem(i, ChooseOption);
			}
	    }
	    for(int i = 9; i<36; i++) {
	    	player.getInventory().setItem(i, ChooseOption);
	    }
	    player.openInventory(inv);
	    
	}
	
	
	@EventHandler
	public void onClickSlot(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		if(event.getCurrentItem() == null) {
			return;
		}
		if (event.getCurrentItem().getType() != Material.AIR) {
			if (event.getInventory().getTitle() == "�eKies je optie!") {
				if (event.getCurrentItem().getItemMeta().getDisplayName() == "�aOptie #1") {
					player.closeInventory();
					player.getInventory().clear();
					player.getInventory().addItem(new ItemStack(Material.BREAD, 4));
				}
				if (event.getCurrentItem().getItemMeta().getDisplayName() == "�aOptie #2") {
					player.closeInventory();
					player.getInventory().clear();
					player.getInventory().addItem(new ItemStack(Material.BREAD, 4));
				}
				if (event.getCurrentItem().getItemMeta().getDisplayName() == "�aOptie #3") {
					player.closeInventory();
					player.getInventory().clear();
					player.getInventory().addItem(new ItemStack(Material.BREAD, 4));
				}
				if (event.getCurrentItem().getItemMeta().getDisplayName() == "�aOptie #4") {
					player.closeInventory();
					player.getInventory().clear();
					player.getInventory().addItem(new ItemStack(Material.BREAD, 4));
				}
				event.setCancelled(true);
			}
			else {
				return;
			}
		}
		else {
			return;
		}
		
	}
}
