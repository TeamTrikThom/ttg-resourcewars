package nl.TrikThom.resourcewars.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import nl.TrikThom.resourcewars.commands.Boosters;
import nl.TrikThom.resourcewars.commands.Craft;
import nl.TrikThom.resourcewars.commands.Reset;
import nl.TrikThom.resourcewars.commands.ResourceWars;
import nl.TrikThom.resourcewars.commands.War;
import nl.TrikThom.resourcewars.data.Config;
import nl.TrikThom.resourcewars.data.EN;
import nl.TrikThom.resourcewars.data.NL;
import nl.TrikThom.resourcewars.data.Nations;
import nl.TrikThom.resourcewars.data.PlayerData;
import nl.TrikThom.resourcewars.data.RankData;
import nl.TrikThom.resourcewars.events.Chat;
import nl.TrikThom.resourcewars.events.Drop;
import nl.TrikThom.resourcewars.events.FirstJoin;
import nl.TrikThom.resourcewars.events.Join;
import nl.TrikThom.resourcewars.events.OpenInv;
import nl.TrikThom.resourcewars.events.PlayerAttack;
import nl.TrikThom.resourcewars.events.Quit;
import nl.TrikThom.resourcewars.invs.StartInv;
import nl.TrikThom.resourcewars.menus.BoosterMenu;
import nl.TrikThom.resourcewars.menus.Characteristics;
import nl.TrikThom.resourcewars.menus.MainMenu;
import nl.TrikThom.resourcewars.menus.StartMenu;

public class Main extends JavaPlugin {
	
	public static Plugin plugin;
	private static PlayerData pd = PlayerData.getInstance();
	private static RankData rd = RankData.getInstance();
	private static NL nl = NL.getInstance();
	private static EN en = EN.getInstance();
	private static Nations ns = Nations.getInstance();
	public static String masterVersion;
	
	
	public void onEnable() {
		
		plugin = this;
		
		try {
	        URL url = new URL("https://www.trikthom.nl/plugins/resourcewars/version.txt");
	        URLConnection connection = url.openConnection();
	        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        masterVersion = in.readLine();
	        in.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		registerData();
		registerEvents();
	    registerCommands();
	    
	}

	public void registerEvents() {
		
		//EVENTS
		Bukkit.getServer().getPluginManager().registerEvents(new Join(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Quit(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Chat(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new OpenInv(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Drop(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new FirstJoin(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerAttack(), this);
		
		
		//MENUS
		Bukkit.getServer().getPluginManager().registerEvents(new StartMenu(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new MainMenu(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Characteristics(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new BoosterMenu(), this);
		
		
		//INVS
		Bukkit.getServer().getPluginManager().registerEvents(new StartInv(), this);
		
		
		
		
		
	}
	public void registerData() {
		pd.setup(this);
	    pd.getPlayerData().options().copyDefaults(true);
	    pd.savePlayerData();
	    
	    rd.setup(this);
	    rd.getRankData().options().copyDefaults(true);
	    rd.saveRankData();
	    
	    nl.setup(this);
	    nl.getNLData().options().copyDefaults(true);
	    nl.saveNLData();
	    
	    en.setup(this);
	    en.getENData().options().copyDefaults(true);
	    en.saveENData();
	    
	    ns.setup(this);
	    ns.getNationsData().options().copyDefaults(true);
	    ns.saveNationsData();
	    
	    
	    Config.registerDefaults();
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	}
	

	public void registerCommands() {
		
		getCommand("reset").setExecutor(new Reset());
		getCommand("resourcewars").setExecutor(new ResourceWars());
		getCommand("war").setExecutor(new War());
		getCommand("boosters").setExecutor(new Boosters());
		getCommand("craft").setExecutor(new Craft());
		
	}
	
}